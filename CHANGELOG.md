# ChangeLog

## 0.1.2

### Changed

- drop `extern crate core;` as core is unused

## 0.1.1

### Added

- Support for more casts on discriminants

## 0.1.0

Initial version
